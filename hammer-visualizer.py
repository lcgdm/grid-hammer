#!/usr/bin/env python3
from __future__ import print_function, division, absolute_import

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import numpy as np
import urlparse
import os
import argparse
import json

OVERWRITE_GRAPHS = False

def allequal(lst):
    return lst.count(lst[0]) == len(lst)

def list_only_files(path):
    return [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

def list_directory(path):
    return [f for f in os.listdir(path)]

def path_join(p1, p2):
    return os.path.join(p1, p2)

def extract_last_chunk(path):
    return os.path.basename(os.path.normpath(path))

def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)

def assert_prefix_and_sort(lst, prefix):
    for item in lst:
        if not item.startswith(prefix):
            raise Exception("Unrecognized item {0} in {1}, was expecting the following format: {2}number".format(item, lst, prefix))

    nums = sorted([int(x[len(prefix):]) for x in lst])
    return ["{0}{1}".format(prefix, num) for num in nums]

def err_text(nerrors, nops):
    if nerrors == 0: return ("No errors", "green")

    percentage = (nerrors / nops) * 100

    if percentage <= 0.5:
        color = "seagreen"
    elif percentage <= 2:
        color = "blue"
    elif percentage <= 5:
        color = "purple"
    else:
        color = "red"

    return ("{0} errors ({1:0.2g}%)".format(nerrors, percentage), color)

def skip_graph(savelocation):
    if OVERWRITE_GRAPHS: return False
    return os.path.exists(savelocation) or os.path.exists("{0}.png".format(savelocation))

def makeHistogram(data, left_text, right_text, xlabel, suptitle, savelocation):
    if skip_graph(savelocation): return

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(xlabel + " in milliseconds")
    ax.set_ylabel("Probability")
    fig.suptitle(suptitle)

    n, bins, patches = ax.hist([x * (10**-6) for x in data], 50, normed=True, facecolor='green', alpha=0.75)
    # TODO: handle outliers?

    plt.figtext(0.99, 0.01, right_text[0], horizontalalignment='right', color=right_text[1])
    plt.figtext(0.01, 0.01, left_text, horizontalalignment='left', color="blue")
    fig.savefig(savelocation)

def makeScatter(x, y, yerr, left_text, right_text, xlabel, suptitle, savelocation):
    if skip_graph(savelocation): return

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(xlabel)
    ax.set_ylabel("Hz")
    fig.suptitle(suptitle)

    ax.errorbar(x, y, yerr=yerr, fmt='-o')

    plt.figtext(0.99, 0.01, right_text[0], horizontalalignment='right', color=right_text[1])
    plt.figtext(0.01, 0.01, left_text, horizontalalignment='left', color="blue")
    fig.savefig(savelocation)

def makeBarChart(x, y, yerr, left_text, right_text, xlabel, suptitle, savelocation):
    if skip_graph(savelocation): return

    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.set_xlabel(xlabel)
    ax.set_ylabel("Hz")
    fig.suptitle(suptitle)

    error_config = {'ecolor': '0.3'}
    bar_width = 0.35

    index = np.arange(len(x))

    rects1 = plt.bar(index, y, bar_width,
                 alpha=0.4,
                 color='b',
                 yerr=yerr,
                 label='lmao')

    plt.xticks(index + bar_width / 2, [str(v) for v in x])
    plt.figtext(0.99, 0.01, right_text[0], horizontalalignment='right', color=right_text[1])
    plt.figtext(0.01, 0.01, left_text, horizontalalignment='left', color="blue")
    fig.savefig(savelocation)

def getargs():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description="Visualizes performance results from tests agaist DPM instances.\n",
                                     epilog="Sample epilog.\n")

    parser.add_argument('--path', type=str, required=True, help="The location where the run results are stored. Can be either a file (when visualizing a single run), or a directory (when aggregating and showing the history for multiple runs)")
    parser.add_argument('--output-dir', type=str, required=True, help="The location where the graphs are to be stored")
    parser.add_argument('--overwrite-graphs', dest='overwrite', action='store_true')
    parser.set_defaults(overwrite=False)

    args = parser.parse_args()

    global OVERWRITE_GRAPHS
    OVERWRITE_GRAPHS = args.overwrite

    return args

def extractLatencyAndErrors(data):
    latencies = []
    errors = []

    for outcome in data["outcomes"]:
            latencies.append(outcome["latency"])

    return (latencies, errors)

class SingleRun(object):
    def __init__(self, filename):
        self.filename = filename
        self.parse()

    def parse(self):
        with open(self.filename) as f:
            self.raw = json.load(f)

        self.__latencies = []
        self.__errors = []

        for outcome in self.raw["outcomes"]:
            self.__latencies += [outcome["latency"]]
            if "error" in outcome: self.__errors += [outcome["error"]]

        self.protocol = urlparse.urlparse(self.raw["options"]["--url"]).scheme
        self.operation = self.raw["options"]["--operation"]
        self.firstfile = self.raw["firstfile"]
        self.lastfile = self.raw["lastfile"]
        self.nthreads = self.raw["nthreads"]
        self.nerrors = len(self.__errors)
        self.nops = int(self.lastfile) - int(self.firstfile)

    def rate(self):
        return len(self.raw["outcomes"]) / (self.raw["duration"] * 10**-9)

    def latencies(self):
        return self.__latencies

    def storeErrors(self, filename):
        if len(self.__errors) == 0: return

        with open(filename, "w") as f:
            for error in self.__errors:
                f.write("{0}\n".format(error))

    def visualize(self, filename):
        makeHistogram(
          self.latencies(),
          "Rate: {0:0.2f} Hz".format(self.rate()),
          err_text(self.nerrors, self.nops),
          "Latency",
          "{0}: latency distribution for a {1} operation, {2} threads, files {3}-{4}".format(self.protocol, self.operation, self.nthreads, self.firstfile, self.lastfile),
          filename
        )

def collect(objects, name):
    collection = [getattr(obj, name) for obj in objects]
    assert len(collection) >= 1
    #assert allequal(collection)
    return collection[0]

def aggregate(objects, name):
    collection = [getattr(obj, name) for obj in objects]
    return sum(collection)

class MultipleRuns(object):
    def __init__(self, dirname):
        self.dirname = dirname
        self.parse()

    def parse(self):
        self.runs = []

        files = list_directory(self.dirname)
        for f in files:
            if not f.startswith("run-"): raise("Unrecognized file/folder when processing multiple runs: {0}".format(f))
            self.runs += [SingleRun(path_join(self.dirname, f))]

        filenames = [int(extract_last_chunk(r.filename)[4:]) for r in self.runs]
        filenames.sort()
        assert filenames == range(1, len(filenames)+1)

        self.nruns = len(self.runs)
        self.protocol = collect(self.runs, "protocol")
        self.nthreads = collect(self.runs, "nthreads")
        self.operation = collect(self.runs, "operation")
        self.firstfile = collect(self.runs, "firstfile")
        self.lastfile = collect(self.runs, "lastfile")

        self.nerrors = aggregate(self.runs, "nerrors")
        self.nops = aggregate(self.runs, "nops")

    def latencies(self):
        ret = []
        for run in self.runs: ret += run.latencies()
        return ret

    def rates(self):
        ret = []
        for run in self.runs: ret += [run.rate()]
        return ret

    def avgrate(self):
        rates = self.rates()
        return sum(rates) / len(rates)

    def ratestd(self):
        return np.std(self.rates())

    def visualize(self, filename):
        makeHistogram(
          self.latencies(),
          u"Rate: {0:0.2f} \u00b1 {1:0.2f} Hz".format(self.avgrate(), self.ratestd()),
          err_text(self.nerrors, self.nops),
          "Latency",
          "{0}: latency distribution for a {1} operation, {2} threads, files {3}-{4}, {5} runs".format(self.protocol, self.operation, self.nthreads, self.firstfile, self.lastfile, self.nruns),
          filename,
        )

    def visualizeAll(self, dirname):
        for run in self.runs:
            run.visualize(path_join(dirname, extract_last_chunk(run.filename)))
            run.storeErrors(path_join(dirname, extract_last_chunk(run.filename) + ".errors"))

        self.visualize(path_join(dirname, "averaged"))

class MultipleThreads(object):
    def __init__(self, dirname):
        self.dirname = dirname
        self.parse()

    def parse(self):
        self.results = []

        folders = list_directory(self.dirname)
        folders = assert_prefix_and_sort(folders, "threads-")

        for f in folders:
            self.results += [MultipleRuns(path_join(self.dirname, f))]

        self.nthreads = [r.nthreads for r in self.results]
        self.protocol = collect(self.results, "protocol")
        self.operation = collect(self.results, "operation")

        self.firstfile = collect(self.results, "firstfile")
        self.lastfile = collect(self.results, "lastfile")
        self.nruns = collect(self.results, "nruns")
        self.nerrors = aggregate(self.results, "nerrors")
        self.nops = aggregate(self.results, "nops")

    def rates(self): return [x.avgrate() for x in self.results]
    def stds(self): return [x.ratestd() for x in self.results]

    def visualizeScatter(self, filename):
        makeScatter(
          self.nthreads,
          self.rates(),
          self.stds(),
          "",
          err_text(self.nerrors, self.nops),
          "Number of threads",
          "{0}: average rate for a {1} operation, files {2}-{3}, {4} runs".format(self.protocol, self.operation, self.firstfile, self.lastfile, self.nruns),
          filename
        )

    def visualizeBarChart(self, filename):
        makeBarChart(
          self.nthreads,
          self.rates(),
          self.stds(),
          "",
          err_text(self.nerrors, self.nops),
          "Number of threads",
          "{0}: average rate for a {1} operation, files {2}-{3}, {4} runs".format(self.protocol, self.operation, self.firstfile, self.lastfile, self.nruns),
          filename
        )

    def visualizeAll(self, dirname):
        self.visualizeScatter(path_join(dirname, "average-scatter"))
        self.visualizeBarChart(path_join(dirname, "average-bar"))

        for result in self.results:
            targetdir = path_join(dirname, extract_last_chunk(result.dirname))
            mkdir(targetdir)
            result.visualizeAll(targetdir)

class MultipleOperations(object):
    def __init__(self, dirname):
        self.dirname = dirname
        self.parse()

    def parse(self):
        self.results = []
        folders = list_directory(self.dirname)
        for f in folders:
            if "hash" in f: continue
            self.results += [MultipleThreads(path_join(self.dirname, f))]

    def visualizeAll(self, dirname):
        for result in self.results:
            targetdir = path_join(dirname, extract_last_chunk(result.dirname))
            mkdir(targetdir)
            result.visualizeAll(targetdir)

class MultipleProtocols(object):
    def __init__(self, dirname):
        self.dirname = dirname
        self.parse()

    def parse(self):
        self.results = []
        folders = list_directory(self.dirname)
        for f in folders:
            self.results += [MultipleOperations(path_join(self.dirname, f))]

    def visualizeAll(self, dirname):
        for result in self.results:
            targetdir = path_join(dirname, extract_last_chunk(result.dirname))
            mkdir(targetdir)
            result.visualizeAll(targetdir)


def visualizeSingleRun(source, destination):
    run = SingleRun(source)
    run.visualize(destination)

def main():
    args = getargs()

    if not os.path.exists(args.path):
        raise Exception("{0} does not exist".format(args.path))

    # Dealing with only a single run - simply make a histogram
    if os.path.isfile(args.path):
        run = SingleRun(args.path)
        run.visualize(path_join(args.output_dir, extract_last_chunk(args.path)))
        return

    results = MultipleProtocols(args.path)
    results.visualizeAll(args.output_dir)

if __name__ == "__main__":
    main()
