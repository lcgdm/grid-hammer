// --------------------------------------------------------------------------
// File: hammer-xroot.cc
// Author: Georgios Bitzes - CERN
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#include <iostream>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <XrdCl/XrdClFile.hh>
#include <XrdCl/XrdClDefaultEnv.hh>

#include "rapidjson/prettywriter.h"
#include "XrdClExecutor.hh"
#include "optionparser.h"
#include "ArgUtils.hh"
#include "Utils.hh"
#include "Defaults.hh"

using namespace rapidjson;

std::vector<option::Option> parse_args(int argc, char** argv) {
  const option::Descriptor usage[] = {
    {Opt::UNKNOWN, 0, "", "", Arg::Unknown, "A load-testing tool for grid storage systems to stress the xroot interface.\nNot for use against production systems.\n"
                                                 "USAGE: xrootd-hammer [options]\n\n" "Options:" },
    {Opt::HELP, 0, "", "help", Arg::None, " --help \tPrint usage and exit" },
    {Opt::URL, 0, "", "url", Arg::NonEmpty, " --url \tThe base path of the tests - the filename will be appended as-is."},
    {Opt::NTHREADS, 0, "", "nthreads", Arg::Numeric, " --nthreads \tthe number of threads to launch"},
    {Opt::FIRSTFILE, 0, "", "firstfile", Arg::Numeric, " --firstfile \trun the test on files from --startfile to --lastfile (default: 0)"},
    {Opt::LASTFILE, 0, "", "lastfile", Arg::Numeric, " --lastfile \trun the test on files from --startfile to --lastfile (default: 10000)"},
    {Opt::MAXINFLIGHT, 0, "", "max-in-flight", Arg::Numeric, " --max-in-flight \tmaximum number of operations in-flight (default: 1000)"},
    {Opt::CERT_PATH, 0, "E", "cert", Arg::NonEmpty, " --cert, -E \tClient certificate in PEM format. If not specified, the standard grid paths will be searched."},
    {Opt::CERT_KEY, 0, "", "key", Arg::NonEmpty, " --key \tPrivate key in PEM format"},
    {Opt::OPERATION, 0, "", "operation", Arg::Operation, " --operation \tThe operation to perform. (stat, read, write, delete)"},
    {Opt::INITIALIZATION, 0, "", "initialization", Arg::Boolean, " --initialization \tIf true, the connections will be initialized in a separate step beforehand, instead of lazily upon thread creation. Useful to measure the impact of setting up the connections, usually originating from TCP handshake + kXR_login. (default: off, lazy initialization)"},
    {0,0,0,0,0,0}
  };

  option::Stats stats(usage, argc, argv);
  std::vector<option::Option> options(stats.options_max);
  std::vector<option::Option> buffer(stats.buffer_max);
  option::Parser parse(usage, argc, argv, &options[0], &buffer[0]);

  std::string err;
  if(!verify_options_sane(parse, options, err)) {
    option::printUsage(std::cerr, usage);
    std::cerr << std::endl;
    if(!err.empty()) {
      std::cerr << std::endl << "Error: " << err << std::endl;
    }
    // hack to have the error message appear at the end, which
    // is more user-friendly
    option::Parser(usage, argc, argv, &options[0], &buffer[0]);
    std::exit(1);
  }
  return options;
}

int main(int argc, char** argv) {
  std::vector<option::Option> opts = parse_args(argc-1, argv+1);

  StringBuffer s;
  PrettyWriter<StringBuffer> writer(s);
  writer.StartObject();

  XrdClExecutor executor(extract_opt(opts[Opt::CERT_PATH]), extract_opt(opts[Opt::CERT_KEY]), "");

  int64_t lastfile = DEFAULT_LASTFILE;
  if(opts[Opt::LASTFILE]) lastfile = atoi(opts[Opt::LASTFILE].arg);

  int64_t firstfile = DEFAULT_FIRSTFILE;
  if(opts[Opt::FIRSTFILE]) firstfile = atoi(opts[Opt::FIRSTFILE].arg);

  int64_t nthreads = DEFAULT_NTHREADS;
  if(opts[Opt::NTHREADS]) nthreads = atoi(opts[Opt::NTHREADS].arg);

  int64_t maxInFlight = DEFAULT_MAXINFLIGHT;
  if(opts[Opt::MAXINFLIGHT]) maxInFlight = atoi(opts[Opt::MAXINFLIGHT].arg);

  bool initialization = DEFAULT_INITIALIZATION;
  if(opts[Opt::INITIALIZATION]) parseBooleanFlag(opts[Opt::INITIALIZATION].arg, initialization);

  std::string proxyEnv, certEnv, keyEnv;

  bool certExists = opts[Opt::CERT_PATH];
  bool keyExists = opts[Opt::CERT_KEY];

  // Proxy certificate?
  if( (certExists && !keyExists) || (certExists && keyExists && opts[Opt::CERT_PATH].arg == opts[Opt::CERT_KEY].arg)) {
    proxyEnv = SSTR("XrdSecGSIUSERPROXY=" << opts[Opt::CERT_PATH].arg);
    putenv( (char*) proxyEnv.c_str());
  }
  else {
    // user cert + key?
    if(opts[Opt::CERT_PATH] && opts[Opt::CERT_KEY]) {
      certEnv = SSTR("XrdSecGSIUSERCERT=" << opts[Opt::CERT_PATH].arg);
      putenv( (char*) certEnv.c_str());

      keyEnv = SSTR("XrdSecGSIUSERKEY=" << opts[Opt::CERT_KEY].arg);
      putenv( (char*) keyEnv.c_str());
    }
  }

  Operation::Type op = stringToOperation(opts[Opt::OPERATION].arg);

  struct timespec startInit, endInit;
  clock_gettime(CLOCK_MONOTONIC, &startInit);
  if(initialization) {
    std::cerr << "Initializing connections..." << std::endl;
    executor.initializeConnections(opts[Opt::URL].arg, nthreads);
  }
  clock_gettime(CLOCK_MONOTONIC, &endInit);

  struct timespec startTime, endTime;
  clock_gettime(CLOCK_MONOTONIC, &startTime);

  XrdCl::Env *clptr = XrdCl::DefaultEnv::GetEnv();
  clptr->PutInt("WorkerThreads", nthreads);

  std::vector<OperationOutcome> outcomes;
  if(op == Operation::LIST) {
    outcomes = executor.performParallelListings(
        opts[Opt::URL].arg,
        lastfile,
        nthreads,
        false
    );
  }
  else if(op == Operation::LISTSTAT) {
    outcomes = executor.performParallelListings(
        opts[Opt::URL].arg,
        lastfile,
        nthreads,
        true
    );
  }
  else {
    outcomes = executor.performManyInParallel(
      op,
      opts[Opt::URL].arg,
      firstfile,
      lastfile,
      maxInFlight,
      nthreads
    );
  }

  clock_gettime(CLOCK_MONOTONIC, &endTime);

  if(initialization) {
    writer.Key("init-duration");
    writer.Int64(time_difference(startInit, endInit));
  }

  writer.Key("duration");
  writer.Int64(time_difference(startTime, endTime));

  writer.Key("firstfile");
  writer.String(SSTR(firstfile).c_str());

  writer.Key("lastfile");
  writer.String(SSTR(lastfile).c_str());

  writer.Key("nthreads");
  writer.String(SSTR(nthreads).c_str());

  writer.Key("operation");
  writer.String(Operation::toString(op).c_str());

  write_args_to_json(argc, argv, writer);
  write_options_to_json(opts, writer);
  write_outcomes_to_json(outcomes, writer);

  writer.EndObject();
  std::cout << s.GetString() << std::endl;
  return 0;
}
