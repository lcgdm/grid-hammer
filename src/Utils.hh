// ----------------------------------------------------------------------
// File: Utils.hh
// Author: Georgios Bitzes - CERN
// ----------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#ifndef __GRID_HAMMER_UTILS_H__
#define __GRID_HAMMER_UTILS_H__

#include <sstream>
#include <string>
#include <iostream>
#include <vector>
#include <sstream>
#include <iomanip>

#include "optionparser.h"

#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#define SSTR(message) static_cast<std::ostringstream&>(std::ostringstream().flush() << message).str()
#define DBG(message) std::cerr << __FILE__ << ":" << __LINE__ << " -- " << #message << " = " << message << std::endl
#define THROW(message) { DBG(message); std::terminate(); }

inline std::string hexEncode(std::string input, std::string separator="") {
  std::ostringstream ss;
  for(std::string::iterator it = input.begin(); it != input.end(); it++) {
    ss << std::setw(2) << std::setfill('0') << std::hex << (unsigned int) ( (unsigned char) *it) << separator;
  }
  return ss.str();
}

namespace Operation {
enum  Type { STAT = 0, READ, WRITE, DELETE, REPAIR, APPEND, LIST, LISTSTAT, INVALID };

inline std::string toString(Type &type) {
  switch(type) {
    case STAT: return "STAT";
    case READ: return "READ";
    case WRITE: return "WRITE";
    case DELETE: return "DELETE";
    case REPAIR: return "REPAIR";
    case APPEND: return "APPEND";
    case LIST: return "LIST";
    case LISTSTAT: return "LISTSTAT";
    case INVALID: return "INVALID";
  }

  THROW("internal error in Operation::toString");
}
}

struct OperationOutcome {
  Operation::Type type;
  int64_t latencyInNs;
  std::string error;
  std::string filename;

  void toWriter(rapidjson::Writer<rapidjson::StringBuffer> &writer) const {
    writer.StartObject();
    writer.Key("latency");
    writer.Int64(latencyInNs);

    if(!error.empty()) {
      writer.Key("error");
      writer.String(error.c_str());
    }

    if(!filename.empty()) {
      writer.Key("filename");
      writer.String(filename.c_str());
    }

    writer.EndObject();
  }
};

inline Operation::Type stringToOperation(const std::string &descr) {
  if(descr == "stat" || descr == "STAT") return Operation::STAT;
  if(descr == "read" || descr == "READ") return Operation::READ;
  if(descr == "write" || descr == "WRITE") return Operation::WRITE;
  if(descr == "delete" || descr == "DELETE") return Operation::DELETE;
  if(descr == "append" || descr == "APPEND") return Operation::APPEND;
  if(descr == "repair" || descr == "REPAIR") return Operation::REPAIR;
  if(descr == "list" || descr == "LIST") return Operation::LIST;
  if(descr == "liststat" || descr == "LISTSTAT") return Operation::LISTSTAT;
  return Operation::INVALID;
}

// Attention: return code denotes whether parsing was successful!
// The result is stored into res
inline bool parseBooleanFlag(const std::string &descr, bool &res) {
  if(descr == "1" || descr == "on" || descr == "yes" || descr == "true") {
    res = true;
    return true;
  }

  if(descr == "0" || descr == "off" || descr == "no" || descr == "false") {
    res = false;
    return true;
  }

  return false;
}

inline int64_t time_difference(struct timespec &start, struct timespec &end) {
  return ((int64_t)end.tv_sec - (int64_t)start.tv_sec)*(int64_t)1000000000 +
         ((int64_t)end.tv_nsec - (int64_t)start.tv_nsec);
}

inline void write_outcomes_to_json(const std::vector<OperationOutcome> &outcomes,
     rapidjson::Writer<rapidjson::StringBuffer> &writer) {
  writer.Key("outcomes");
  writer.StartArray();
  for(size_t i = 0; i < outcomes.size(); i++) {
    outcomes[i].toWriter(writer);
  }
  writer.EndArray();
}

inline void write_options_to_json(const std::vector<option::Option> &opts, rapidjson::Writer<rapidjson::StringBuffer> &writer) {
  writer.Key("options");
  writer.StartObject();
  for(size_t i = 0; i < opts.size(); i++) {
    if(opts[i]) {
      writer.Key(opts[i].name);
      if(opts[i].arg) {
        writer.String(opts[i].arg);
      }
      else {
        writer.String("");
      }
    }
  }
  writer.EndObject();
}

inline void write_args_to_json(int argc, char** argv, rapidjson::Writer<rapidjson::StringBuffer> &writer) {
  writer.Key("args");
  writer.StartArray();

  for(int i = 0; i < argc; i++) {
    writer.String(argv[i]);
  }
  writer.EndArray();
}

inline std::string chopFilename(const std::string &fullName) {
  size_t startIndex = 0u;

  for(size_t i = fullName.size() - 1; i > 0; i--) {

    if(fullName[i] == '/') {
      startIndex = i+1;
      break;
    }
  }

  return fullName.substr(startIndex);
}

namespace Opt {
enum Type { START = 0,
  HELP, URL, NTHREADS, UNKNOWN, CERT_PATH, CERT_KEY, OPERATION, FIRSTFILE, LASTFILE, MAXINFLIGHT, INITIALIZATION,
  END };
}

inline bool verify_options_sane(option::Parser &parse, std::vector<option::Option> &options, std::string &err) {
  if(parse.error()) {
    return false;
  }

  if(options[Opt::HELP]) {
    return false;
  }

  if(!options[Opt::URL]) {
    err = "--url is required.";
    return false;
  }

  if(!options[Opt::OPERATION]) {
    err = "--operation is required.";
    return false;
  }

  for(int i = Opt::START; i < Opt::END; i++) {
    if(options[i].count() > 1) {
      err = "all options can only be specified once";
      return false;
    }
  }

  for(int i = 0; i < parse.nonOptionsCount(); ++i) {
    err = SSTR("Non-option #" << i << ": " << parse.nonOption(i));
    return false;
  }

  return true;
}

#endif
