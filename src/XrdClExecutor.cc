// --------------------------------------------------------------------------
// File: XrdClExecutor.cc
// Author: Georgios Bitzes - CERN
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#include "XrdClExecutor.hh"
#include <XrdCl/XrdClFileSystem.hh>
#include <XrdCl/XrdClFile.hh>
#include <boost/thread/thread.hpp>

XrdClExecutor::XrdClExecutor(const std::string &cert_, const std::string &key_, const std::string &password_)
: cert(cert_), key(key_), password(password_) {
}

XrdClExecutor::~XrdClExecutor() {
}

enum HandlerState {
  UNINITIALIZED = 0,
  PENDING_OPEN,
  PENDING_MAIN,
  PENDING_CLOSE,
  FINALIZED
};

class HandlerWrapper {
public:
  HandlerWrapper(const XrdCl::URL &url_, ProgressTracker &tracker_)
  : url(url_), state(UNINITIALIZED), progressTracker(tracker_) {
    outcome.filename = chopFilename(url.GetURL());
    clock_gettime(CLOCK_MONOTONIC, &start);
  }

  virtual ~HandlerWrapper() {
    if(state != FINALIZED) {
      outcome.error = SSTR("Error during " << Operation::toString(outcome.type) << " for '" << url.GetURL() << "'. Not finalized.");
      promise.set_value(outcome);
      progressTracker.addFailure();
    }
  }

  void finalize(XrdCl::XRootDStatus *status, XrdCl::AnyObject *response) {
    finalize_internal(*status);

    delete status;
    if(response) delete response;
    delete this;
  }

  void finalize(XrdCl::XRootDStatus &status, XrdCl::XRootDStatus *prevstatus, XrdCl::AnyObject *response) {
    finalize_internal(status);

    delete prevstatus;
    if(response) delete response;
    delete this;
  }

  void checkStatus(const XrdCl::XRootDStatus &status) {
    if(!status.IsOK()) {
      finalize_internal(status);
      delete this;
    }
  }

  OperationOutcome outcome;
  XrdCl::URL url;
private:
  void finalize_internal(const XrdCl::XRootDStatus &status) {
    if(state == FINALIZED) {
      DBG("Bug, asked to finalize a handler twice!");
      std::terminate();
    }

    state = FINALIZED;
    struct timespec end;

    clock_gettime(CLOCK_MONOTONIC, &end);
    outcome.latencyInNs = time_difference(start, end);

    if(!status.IsOK()) outcome.error += status.ToString();
    if(!outcome.error.empty()) outcome.error = SSTR("Error during " << Operation::toString(outcome.type) << " for '" << url.GetURL() << "'. " << outcome.error);

    promise.set_value(outcome);
    if(outcome.error.empty()) {
      progressTracker.addSuccess();
    }
    else {
      progressTracker.addFailure();
    }
  }
protected:
  HandlerState state;
  ProgressTracker &progressTracker;
  struct timespec start;
  boost::promise<OperationOutcome> promise;
};

class StatHandler : public XrdCl::ResponseHandler, public HandlerWrapper {
public:
  StatHandler(const XrdCl::URL &url_, const std::string &expected, ProgressTracker &progressTracker)
  :  HandlerWrapper(url_, progressTracker), fs(url.GetURL()), expectedSize(expected.size()) {
    outcome.type = Operation::STAT;
  }

  virtual ~StatHandler() { }

  boost::unique_future<OperationOutcome> initiate() {
    // After the asynchronous operation, it's not safe to access any members
    // of the class, as the response may have arrived in the meantime, and
    // the object has been destroyed already! Get the future *before* the operation.
    boost::unique_future<OperationOutcome> fut = promise.get_future();
    // async stat
    checkStatus(fs.Stat(url.GetPath(), this));
    return fut;
  }

  virtual void HandleResponse(XrdCl::XRootDStatus *status, XrdCl::AnyObject *response) {
    if(!status->IsOK()) return finalize(status, response);

    XrdCl::StatInfo *stat;
    response->Get(stat);
    response->Set( (int*) 0);

    if(stat->GetSize() != expectedSize) {
      outcome.error = SSTR("File size mismatch for " << url.GetURL() << ": '" << stat->GetSize() << ", expected " << expectedSize);
    }
    delete stat;
    return finalize(status, response);
  }
private:
  XrdCl::FileSystem fs;
  size_t expectedSize;
};

class DeleteHandler : public XrdCl::ResponseHandler, public HandlerWrapper {
public:
  DeleteHandler(const XrdCl::URL &url_, ProgressTracker &progressTracker)
    : HandlerWrapper(url_, progressTracker), fs(url.GetURL()) {
    outcome.type = Operation::DELETE;
  }

  boost::unique_future<OperationOutcome> initiate() {
    // After the asynchronous operation, it's not safe to access any members
    // of the class, as the response may have arrived in the meantime, and
    // the object has been destroyed already! Get the future *before* the operation.
    boost::unique_future<OperationOutcome> fut = promise.get_future();

    // async delete
    checkStatus(fs.Rm(url.GetPath(), this));
    return fut;
  }

  virtual void HandleResponse(XrdCl::XRootDStatus *status, XrdCl::AnyObject *response) {
    return finalize(status, response);
  }

private:
  XrdCl::FileSystem fs;
};

class ReadHandler : public XrdCl::ResponseHandler, public HandlerWrapper {
public:
  ReadHandler(const XrdCl::URL &url_, const std::string &expected, ProgressTracker &progressTracker)
  :  HandlerWrapper(url_, progressTracker), expectedContent(expected) {
    outcome.type = Operation::READ;
    buffer = (char*) malloc(sizeof(char) * (expectedContent.size()+20)  );
  }

  boost::unique_future<OperationOutcome> initiate() {
    // After the asynchronous operation, it's not safe to access any members
    // of the class, as the response may have arrived in the meantime, and
    // the object has been destroyed already! Get the future *before* the operation.
    boost::unique_future<OperationOutcome> fut = promise.get_future();

    // async open
    state = PENDING_OPEN;
    checkStatus(file.Open(url.GetURL(), XrdCl::OpenFlags::Read, XrdCl::Access::None, this));
    return fut;
  }

  virtual ~ReadHandler() {
    free(buffer);
  }

  virtual void HandleResponse(XrdCl::XRootDStatus *status, XrdCl::AnyObject *response) {
    if(!status->IsOK()) {
      return finalize(status, response);
    }

    if(state == PENDING_OPEN) {
      state = PENDING_MAIN;

      // async read
      XrdCl::XRootDStatus st = file.Read(0, expectedContent.size()+10, buffer, this);
      if(!st.IsOK()) {
        return finalize(st, status, response);
      }
    }
    else if(state == PENDING_MAIN) {
      if(!status->IsOK()) {
        outcome.error += status->ToString();
      }
      else {
        // check result
        XrdCl::ChunkInfo *chunk;
        response->Get(chunk);
        response->Set( (int*) 0);
        size_t bytesRead = chunk->length;
        delete chunk;

        std::string receivedContent(buffer, bytesRead);
        if(expectedContent != receivedContent) {
          outcome.error += SSTR("File content mismatch for " << url.GetURL() << ". Received bytes: '" << hexEncode(receivedContent, " ") << "'. Expected bytes: '" << hexEncode(expectedContent, " ") << "'");
        }
      }

      state = PENDING_CLOSE;
      XrdCl::XRootDStatus st = file.Close(this);
      if(!st.IsOK()) return finalize(st, status, response);
    }
    else if(state == PENDING_CLOSE) {
      return finalize(status, response);
    }
    else {
      THROW("Bug, should never reach here");
    }
  }
private:
  XrdCl::File file;

  char *buffer;
  std::string expectedContent;
  size_t expectedSize;
};

class WriteHandler : public XrdCl::ResponseHandler, public HandlerWrapper {
public:
  WriteHandler(const XrdCl::URL &url_, bool append_, const std::string &content_, ProgressTracker &progressTracker)
  :  HandlerWrapper(url_, progressTracker), append(append_), content(content_) {
    outcome.type = Operation::WRITE;
  }

  boost::unique_future<OperationOutcome> initiate() {
    // After the asynchronous operation, it's not safe to access any members
    // of the class, as the response may have arrived in the meantime, and
    // the object has been destroyed already! Get the future *before* the operation.
    boost::unique_future<OperationOutcome> fut = promise.get_future();

    // async open
    state = PENDING_OPEN;

    XrdCl::OpenFlags::Flags openflags;
    if(append) {
      openflags = XrdCl::OpenFlags::Update; //  | XrdCl::OpenFlags::Update;
    }
    else {
      // create new file
      openflags = XrdCl::OpenFlags::New;
    }

    checkStatus(file.Open(url.GetURL(), openflags, XrdCl::Access::None, this));
    return fut;
  }

  virtual void HandleResponse(XrdCl::XRootDStatus *status, XrdCl::AnyObject *response) {
    if(!status->IsOK()) {
      outcome.error = status->ToString();
      return finalize(status, response);
    }

    if(state == PENDING_OPEN) {
      // async write
      state = PENDING_MAIN;

      off_t offset = 0;
      if(append) {
        offset = 300;
      }

      XrdCl::XRootDStatus st = file.Write(offset, content.size(), content.c_str(), this);
      if(!st.IsOK()) return finalize(st, status, response);
    }
    else if(state == PENDING_MAIN) {
      if(!status->IsOK()) {
        outcome.error = status->ToString();
      }

      state = PENDING_CLOSE;
      XrdCl::XRootDStatus st = file.Close(this);
      if(!st.IsOK()) return finalize(st, status, response);
    }
    else if(state == PENDING_CLOSE) {
      return finalize(status, response);
    }
    else {
      DBG("Bug, should never reach here");
      std::terminate();
    }
  }
private:
  XrdCl::File file;
  bool append;
  std::string content;
};

class ListingHandler : public HandlerWrapper {
public:
  ListingHandler(const XrdCl::URL &url_, size_t threadID, ProgressTracker &progressTracker, size_t expected, bool stat)
  : HandlerWrapper(url_, progressTracker), mFilesystem(url_), expectedEntries(expected), mStat(stat) {

    outcome.type = Operation::LIST;
    outcome.filename = SSTR("thread-" << threadID);
  }

  boost::unique_future<OperationOutcome> initiate() {
    boost::unique_future<OperationOutcome> future = promise.get_future();
    boost::thread(&ListingHandler::mainThread, this).detach();
    return future;
  }

  void mainThread() {
    XrdCl::DirectoryList *list;

    XrdCl::DirListFlags::Flags flags = XrdCl::DirListFlags::None;
    if(mStat) {
      flags = XrdCl::DirListFlags::Stat;
    }

    XrdCl::XRootDStatus status = mFilesystem.DirList(url.GetPath(), flags, list);

    if(!status.IsOK()) {
      return finalize(status, NULL, NULL);
    }
 
    size_t count = 0;
    for(XrdCl::DirectoryList::Iterator it = list->Begin(); it != list->End(); ++it) {
      count++;
    }

    if(count != expectedEntries) {
      outcome.error += SSTR("File-count mismatch for " << url.GetURL() << ". Expected number of files: " << expectedEntries << ", actual entries: " << count);
    }

    return finalize(status, NULL, NULL);
  }

private:
  XrdCl::FileSystem mFilesystem;
  size_t expectedEntries;
  bool mStat;
};

boost::unique_future<OperationOutcome> XrdClExecutor::performSingle(
    Operation::Type op,
    const std::string &filename,
    const std::string &expectedContent,
    const int threadID
) {

  XrdCl::URL url(filename);
  url.SetUserName(SSTR("thread-" << threadID));

  switch(op) {
    case Operation::READ: {
      ReadHandler *handler = new ReadHandler(url, expectedContent, progressTracker);
      return handler->initiate();
    }
    case Operation::WRITE: {
      WriteHandler *handler = new WriteHandler(url, false, expectedContent, progressTracker);
      return handler->initiate();
    }
    case Operation::STAT: {
      StatHandler *handler = new StatHandler(url, expectedContent, progressTracker);
      return handler->initiate();
    }
    case Operation::APPEND: {
      WriteHandler *handler = new WriteHandler(url, true, "Sample string to append to the end of file.", progressTracker);
      return handler->initiate();
    }
    case Operation::DELETE: {
      DeleteHandler *handler = new DeleteHandler(url, progressTracker);
      return handler->initiate();
    }
    default:
      THROW("operation not implemented");
  }

  THROW("should never reach here");
}

boost::unique_future<OperationOutcome> XrdClExecutor::performSingleListing(const std::string &filename,
    size_t expectedFiles, const int threadID, bool stat) {

  XrdCl::URL url(filename);
  url.SetUserName(SSTR("thread-" << threadID));

  ListingHandler *handler = new ListingHandler(url, threadID, progressTracker, expectedFiles, stat);
  return handler->initiate();
}

