// --------------------------------------------------------------------------
// File: GfalExecutor.hh
// Author: Petr Vokac
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2018 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#ifndef __GRID_HAMMER_GFAL_EXECUTOR_H__
#define __GRID_HAMMER_GFAL_EXECUTOR_H__

#include "OperationExecutor.hh"
#include <gfal_api.h>

class GfalInstance {
public:
  GfalInstance();
  ~GfalInstance();
  gfal2_context_t context;
  gfalt_params_t params;
};

class GfalExecutor : public OperationExecutor {
public:
  ~GfalExecutor();
  virtual boost::unique_future<OperationOutcome> performSingle(
    Operation::Type op,
    const std::string &filename,
    const std::string &expectedContent,
    const int threadID
  );

  virtual boost::unique_future<OperationOutcome> performSingleListing(
    const std::string &filename,
    size_t expectedFiles,
    const int threadID,
    bool stat
  );

  GfalExecutor(
    const std::string &cert,
    const std::string &key,
    const std::string &password
  );

private:
  std::string cert, key, password;

  GfalInstance* createGfalInstance();
  GfalInstance* obtainGfalInstance();
  void releaseGfalInstance(GfalInstance *instance);

  std::deque<GfalInstance*> gfalPool;
  std::mutex poolMtx;
};


#endif
