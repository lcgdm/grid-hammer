// --------------------------------------------------------------------------
// File: DavixExecutor.cc
// Author: Georgios Bitzes - CERN
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#include "DavixExecutor.hh"

DavixExecutor::DavixExecutor(const std::string &cert_, const std::string &key_, const std::string &password_)
: cert(cert_), key(key_), password(password_) {
}

DavixExecutor::~DavixExecutor() {
  while(!davixPool.empty()) {
    delete davixPool.front();
    davixPool.pop_front();
  }
}

DavixInstance *DavixExecutor::createDavixInstance() {
  DavixInstance *davixInst = new DavixInstance();

  if(!cert.empty()) {
    Davix::X509Credential creds;
    Davix::DavixError *err = NULL;

    std::string privateKey = cert;
    if(!key.empty()) {
      privateKey = key;
    }

    creds.loadFromFilePEM(privateKey, cert, password, &err);
    if(err) {
      std::cerr << err->getErrMsg() << std::endl;
      std::exit(1);
    }

    davixInst->params.setClientCertX509(creds);
    davixInst->params.setSSLCAcheck(false);
    davixInst->params.setKeepAlive(true);

  }
  else {
    davixInst->context.loadModule("grid");
  }

  return davixInst;
}

DavixInstance* DavixExecutor::obtainDavixInstance() {

  {
    std::unique_lock<std::mutex> lock(poolMtx);

    if(!davixPool.empty()) {
      DavixInstance* ret = davixPool.front();
      davixPool.pop_front();
      return ret;
    }
  }

  return createDavixInstance();
}

void DavixExecutor::releaseDavixInstance(DavixInstance *davixInst) {
  std::lock_guard<std::mutex> lock(poolMtx);
  davixPool.push_back(davixInst);
}

boost::unique_future<OperationOutcome> DavixExecutor::performSingle(
    Operation::Type op,
    const std::string &filename,
    const std::string &expectedContent,
    const int _
) {
  OperationOutcome ret;
  ret.filename = chopFilename(filename);
  DavixInstance *davixInst = this->obtainDavixInstance();
  Davix::DavixError *err = NULL;
  Davix::Uri uri(filename);
  Davix::DavFile file(davixInst->context, uri);
  struct timespec start;
  clock_gettime(CLOCK_MONOTONIC, &start);
  switch(op) {
    case Operation::READ: {
      std::vector<char> buffer;
      file.getFull(&davixInst->params, buffer, &err);
      std::string content(&buffer[0], buffer.size());
      if(!err && content != expectedContent) {
        ret.error = SSTR("File content mismatch for " << filename << ": '" << content << "'. Expected: '" << expectedContent << "'");
      }
      break;
    }
    case Operation::WRITE: {
      try {
        file.put(&davixInst->params, expectedContent.c_str(), expectedContent.size());
      }
      catch(Davix::DavixException &exc) {
        exc.toDavixError(&err);
      }
      break;
    }
    case Operation::STAT: {
      struct stat st;
      file.stat(&davixInst->params, &st, &err);
      if(!err && st.st_size != (off_t) expectedContent.size()) {
        ret.error = SSTR("File size mismatch for " << filename << ": '" << st.st_size << ", expected " << expectedContent.size());
      }
      break;
    }
    case Operation::DELETE: {
      try {
        file.deletion(&davixInst->params);
      }
      catch(Davix::DavixException &exc) {
        exc.toDavixError(&err);
      }
      break;
    }
    case Operation::REPAIR: {
      // try to read
      std::vector<char> buffer;
      file.getFull(&davixInst->params, buffer, &err);
      std::string content(&buffer[0], buffer.size());
      if(err || content != expectedContent) {
        Davix::DavixError::clearError(&err);
        // try to delete, ignore outcome
        this->performSingle(Operation::DELETE, filename, expectedContent, 0);
        // now write again
        try {
          file.put(&davixInst->params, expectedContent.c_str(), expectedContent.size());
        }
        catch(Davix::DavixException &exc) {
          exc.toDavixError(&err);
        }
      }
      break;
    }
    default: {
      THROW("received invalid operation, should never happen");
    }
  }
  struct timespec end;
  clock_gettime(CLOCK_MONOTONIC, &end);
  ret.type = op;
  ret.latencyInNs = time_difference(start, end);
  if(err || !ret.error.empty()) ret.error = SSTR("Error during " << Operation::toString(op) << " for '" << filename << "'. " << ret.error);
  if(err) ret.error += SSTR("DavixError: " << err->getErrMsg());
  Davix::DavixError::clearError(&err);
  this->releaseDavixInstance(davixInst);
  boost::promise<OperationOutcome> promise;
  promise.set_value(ret);
  if(ret.error.empty()) {
    progressTracker.addSuccess();
  }
  else {
    progressTracker.addFailure();
  }
  return promise.get_future();
}

boost::unique_future<OperationOutcome> DavixExecutor::performSingleListing(const std::string &filename,
    size_t expectedFiles, const int threadID, bool stat) {

  THROW("listing benchmark not yet implemented for http");
}
