// --------------------------------------------------------------------------
// File: GfalExecutor.cc
// Author: Petr Vokac
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2018 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#include "GfalExecutor.hh"

GfalInstance::GfalInstance()
: context(NULL), params(NULL) {
  // Errors will be put here
  GError* error = NULL;

  // Create a gfal2 context
  context = gfal2_context_new(&error);
  if (error) {
    std::cerr << "Could not create the gfal2 context: (" << error->code << ")" << error->message << std::endl;
    std::exit(1);
  }

  // Set parameters for the copy
  params = gfalt_params_handle_new(&error);
  if (error) {
    std::cerr << "Could not create the gfal2 transfer parameters: (" << error->code << ")" << error->message << std::endl;
    std::exit(1);
  }

  gfalt_set_timeout(params, 60, &error);
  //gfalt_set_dst_spacetoken(params, "TOKEN", &error);
  gfalt_set_replace_existing_file(params, TRUE, &error);
  gfalt_set_checksum(params, GFALT_CHECKSUM_NONE, NULL, NULL, NULL);
  //gfalt_set_create_parent_dir(params, TRUE, &error);

  // Callbacks
  //gfalt_add_event_callback(params, my_event_callback, NULL, NULL, &error);     // Called when some event is triggered
  //gfalt_add_monitor_callback(params, my_monitor_callback, NULL, NULL, &error); // Performance monitor
}

GfalInstance::~GfalInstance() {
  GError* error = NULL;

  if (params) {
    gfalt_params_handle_delete(params, &error);
  }

  if (context) {
    gfal2_context_free(context);
  }
}

GfalExecutor::GfalExecutor(const std::string &cert_, const std::string &key_, const std::string &password_)
: cert(cert_), key(key_), password(password_) {
}

GfalExecutor::~GfalExecutor() {
  while(!gfalPool.empty()) {
    delete gfalPool.front();
    gfalPool.pop_front();
  }
}

GfalInstance *GfalExecutor::createGfalInstance() {
  GfalInstance *gfalInst = new GfalInstance();

  GError* error = NULL;

  if(!cert.empty()) {
    gfal2_set_opt_string(gfalInst->context, "X509", "CERT", cert.c_str(), &error);
  }
  if(!key.empty()) {
    gfal2_set_opt_string(gfalInst->context, "X509", "KEY", key.c_str(), &error);
  }

  return gfalInst;
}

GfalInstance* GfalExecutor::obtainGfalInstance() {

  {
    std::unique_lock<std::mutex> lock(poolMtx);

    if(!gfalPool.empty()) {
      GfalInstance* ret = gfalPool.front();
      gfalPool.pop_front();
      return ret;
    }
  }

  return createGfalInstance();
}

void GfalExecutor::releaseGfalInstance(GfalInstance *gfalInst) {
  std::lock_guard<std::mutex> lock(poolMtx);
  gfalPool.push_back(gfalInst);
}

boost::unique_future<OperationOutcome> GfalExecutor::performSingle(
    Operation::Type op,
    const std::string &filename,
    const std::string &expectedContent,
    const int _
) {
  OperationOutcome ret;
  ret.filename = chopFilename(filename);
  GfalInstance *gfalInst = this->obtainGfalInstance();
  GError* error = NULL;

  struct timespec start;
  clock_gettime(CLOCK_MONOTONIC, &start);
  switch(op) {
    case Operation::READ: {
      char buffer[expectedContent.size()];
      int fd = gfal2_open(gfalInst->context, filename.c_str(), O_RDONLY, &error);
      gfal2_read(gfalInst->context, fd, &buffer, expectedContent.size(), &error);
      std::string content(&buffer[0], expectedContent.size());
      gfal2_close(gfalInst->context, fd, &error);
      if(!error && content != expectedContent) {
        ret.error = SSTR("File content mismatch for " << filename << ": '" << content << "'. Expected: '" << expectedContent << "'");
      }
      break;
    }
    case Operation::WRITE: {
      int fd = gfal2_open(gfalInst->context, filename.c_str(), O_WRONLY | O_CREAT, &error);
      gfal2_write(gfalInst->context, fd, expectedContent.c_str(), expectedContent.size(), &error);
      gfal2_close(gfalInst->context, fd, &error);
      break;
    }
    case Operation::STAT: {
      struct stat st;
      gfal2_stat(gfalInst->context, filename.c_str(), &st, &error);
      if (!error && st.st_size != (off_t) expectedContent.size()) {
        ret.error = SSTR("File size mismatch for " << filename << ": '" << st.st_size << ", expected " << expectedContent.size());
      }
      break;
    }
    case Operation::DELETE: {
      gfal2_unlink(gfalInst->context, filename.c_str(), &error);
      break;
    }
    default: {
      THROW("received invalid operation, should never happen");
    }
  }
  struct timespec end;
  clock_gettime(CLOCK_MONOTONIC, &end);
  ret.type = op;
  ret.latencyInNs = time_difference(start, end);
  if(error || !ret.error.empty()) ret.error = SSTR("Error during " << Operation::toString(op) << " for '" << filename << "'. " << ret.error);
  if(error) ret.error += SSTR("GfalError: code " << error->code << ", " << error->message);
  this->releaseGfalInstance(gfalInst);
  boost::promise<OperationOutcome> promise;
  promise.set_value(ret);
  if(ret.error.empty()) {
    progressTracker.addSuccess();
  }
  else {
    progressTracker.addFailure();
  }
  return promise.get_future();
}

boost::unique_future<OperationOutcome> GfalExecutor::performSingleListing(const std::string &filename,
    size_t expectedFiles, const int threadID, bool stat) {

  THROW("listing benchmark not yet implemented for gfal");
}

