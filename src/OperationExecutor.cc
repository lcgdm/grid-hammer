// --------------------------------------------------------------------------
// File: OperationExecutor.cc
// Author: Georgios Bitzes - CERN
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#include <thread>
#include <condition_variable>
#include <chrono>

#include "OperationExecutor.hh"

class StatusPrinter {
public:
  StatusPrinter(size_t start_, size_t end_, Counter &processed_, ProgressTracker &tracker_)
  : start(start_), end(end_), processed(processed_), progressTracker(tracker_),
    terminate(0), thread(&StatusPrinter::loop, this) {

  }

  void loop() {
    while(true) {
      std::unique_lock<std::mutex> lock(mtx);
      cond.wait_for(lock, std::chrono::seconds(1));

      if(terminate.get() > 0) break;

      int64_t current = processed.get();
      if(current >= (int64_t) end) current = end;
      int64_t successes = progressTracker.getSuccesses();
      int64_t failures = progressTracker.getFailures();

      std::cerr << "Pending: " << end - current << ", in-flight: " << progressTracker.getInFlight() << ", succeeded: " <<  successes;
      if(failures > 0) {
        std::cerr << ", failed: " << failures;
      }
      std::cerr << std::endl;
    }
  }

  void terminate_and_join() {
    terminate.getAndIncrement();
    cond.notify_all();
    thread.join();
  }

private:
  size_t start;
  size_t end;
  Counter &processed;
  ProgressTracker &progressTracker;

  int numdigits;

  Counter terminate;
  std::mutex mtx;
  std::condition_variable cond;
  std::thread thread;
};

std::vector<OperationOutcome> OperationExecutor::performManyInParallel(
    Operation::Type op,
    const std::string &base,
    size_t start,
    size_t end,
    size_t maxInFlight,
    size_t nthreads) {

  fileIndex.reset(start);
  progressTracker.reset(maxInFlight);

  std::mutex mtx;
  std::vector<std::thread> group;

  std::vector<OperationOutcome> outcomes;

  for(size_t i = 0; i < nthreads; i++) {
    group.emplace_back(&OperationExecutor::performManySingleThread, this, i, op, base, end, std::ref(mtx), std::ref(outcomes));
  }

  StatusPrinter printer(start, end, fileIndex, progressTracker);
  for(size_t i = 0; i < group.size(); i++) {
    group[i].join();
  }
  printer.terminate_and_join();
  return outcomes;
}

std::vector<OperationOutcome> OperationExecutor::performParallelListings(
    const std::string &base,
    size_t expectedFiles,
    size_t nthreads,
    bool stat
  ) {

  std::vector<boost::unique_future<OperationOutcome> > futures;
  for(size_t threadID = 0; threadID < nthreads; threadID++) {
    futures.push_back(this->performSingleListing(base, expectedFiles, threadID, stat));
  }

  std::vector<OperationOutcome> outcomes;
  for(size_t i = 0; i < futures.size(); i++) {
    outcomes.emplace_back(futures[i].get());
  }

  return outcomes;
}

void OperationExecutor::performManySingleThread(
    int threadID,
    Operation::Type op,
    const std::string &base,
    size_t end,
    std::mutex &mtx,
    std::vector<OperationOutcome> &outcomes
  ) {

  std::vector<boost::unique_future<OperationOutcome> > futures;

  while(true) {
    int64_t fileNumber = fileIndex.getAndIncrement();
    if(fileNumber >= (int) end) break;

    std::string expectedContent = SSTR("We hope you will enjoy the content of file " << fileNumber << "\nDon't forget that the quick brown fox jumped over the lazy dog\n");
    progressTracker.addInFlight();
    futures.push_back(this->performSingle(op, SSTR(base << fileNumber), expectedContent, threadID));
  }

  for(size_t i = 0; i < futures.size(); i++) {
    std::lock_guard<std::mutex> lock(mtx);
    outcomes.push_back(futures[i].get());
  }
}

void OperationExecutor::initializeConnections(
    const std::string &base,
    size_t nthreads
  ) {

  // Ignore the outcomes for now - maybe in the future we might want to check
  // them?
  std::vector<boost::unique_future<OperationOutcome>> outcomes;
  for(size_t i = 0; i < nthreads; i++) {
    outcomes.push_back(performSingle(Operation::STAT, base, "", i));
  }

  for(size_t i = 0; i < nthreads; i++) {
    outcomes[i].get();
  }
}
