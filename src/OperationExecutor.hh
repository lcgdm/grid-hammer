// ----------------------------------------------------------------------
// File: OperationExecutor.hh
// Author: Georgios Bitzes - CERN
// ----------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#ifndef __GRID_HAMMER_OPERATION_EXECUTOR_H__
#define __GRID_HAMMER_OPERATION_EXECUTOR_H__

#include "Utils.hh"
#include "Counter.hh"
#include <mutex>
#include <condition_variable>

#include <boost/thread/future.hpp>
#include <XrdCl/XrdClFileSystem.hh>

class ProgressTracker {
public:
  ProgressTracker() {}

  void reset(int64_t maxInFlight_) {
    maxInFlight = maxInFlight_;

    inFlight.reset(0);
    successes.reset(0);
    failures.reset(0);
  }

  int64_t getSuccesses() {
    return successes.get();
  }

  int64_t getFailures() {
    return failures.get();
  }

  int64_t getInFlight() {
    return inFlight.get();
  }

  void addInFlight() {
    std::unique_lock<std::mutex> lock(mtx);

    while(inFlight.get() >= maxInFlight) {
      cv.wait_for(lock, std::chrono::milliseconds(10));
    }

    inFlight.getAndIncrement();
  }

  int64_t addSuccess() {
    std::lock_guard<std::mutex> lock(mtx);
    inFlight.getAndDecrement();
    cv.notify_one();
    return successes.getAndIncrement();
  }

  int64_t addFailure() {
    std::lock_guard<std::mutex> lock(mtx);
    inFlight.getAndDecrement();
    cv.notify_one();
    return failures.getAndIncrement();
  }

private:
  std::mutex mtx;
  std::condition_variable cv;
  int64_t maxInFlight;

  Counter inFlight;
  Counter successes;
  Counter failures;
};

class OperationExecutor {
public:

  virtual boost::unique_future<OperationOutcome> performSingle(
    Operation::Type op,
    const std::string &filename,
    const std::string &expectedContent,
    const int threadID
  ) = 0;

  virtual boost::unique_future<OperationOutcome> performSingleListing(
    const std::string &filename,
    size_t expectedFiles,
    const int threadID,
    bool stat
  ) = 0;

  void initializeConnections(
    const std::string &base,
    size_t nthreads
  );

  std::vector<OperationOutcome> performManyInParallel(
    Operation::Type op,
    const std::string &base,
    size_t start,
    size_t end,
    size_t maxInFlight,
    size_t nthreads
  );

  std::vector<OperationOutcome> performParallelListings(
    const std::string &base,
    size_t expectedNumber,
    size_t nthreads,
    bool stat
  );

  void performManySingleThread(
    int threadID,
    Operation::Type op,
    const std::string &base,
    size_t start,
    std::mutex &mtx,
    std::vector<OperationOutcome> &outcomes
  );
protected:
  Counter fileIndex;

  ProgressTracker progressTracker;
};

#endif
