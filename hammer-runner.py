#!/usr/bin/env python3
from __future__ import print_function, division, absolute_import

import argparse
import os.path
import distutils.spawn
import sys
import threading
import subprocess
import datetime
import textwrap
import json
import signal
import time
import re
import select
import platform
import hashlib
import random
import math
import copy
from pipes import quote

try: ## Python2
    from StringIO import StringIO
except ImportError: ## Python3
    from io import StringIO

VERBOSITY = 0
USE_COLORS = True
ADVANCED_TTY = True
TICKING_INTERVAL = 1
TTY_WIDTH = None
NO_TICKING = False

class Color(object):
    sequences = {
        "red"      : "\033[91;1m",
        "green"    : "\033[92;1m",
        "yellow"   : "\033[93;1m",
        "blue"     : "\033[94;1m",
        "purple"   : "\033[95;1m",
        "cyan"     : "\033[96;1m",
        "gray"     : "\033[97;1m",

        "end"      : "\033[0m"
    }

    @staticmethod
    def colorize(c, s):
        if not USE_COLORS:
            return str(s)
        return Color.sequences[c]+str(s)+Color.sequences["end"]

    @staticmethod
    def red(s)   : return Color.colorize("red", s)

    @staticmethod
    def green(s) : return Color.colorize("green", s)

    @staticmethod
    def yellow(s) : return Color.colorize("yellow", s)

    @staticmethod
    def purple(s): return Color.colorize("purple", s)

    @staticmethod
    def cyan(s): return Color.colorize("cyan", s)

    @staticmethod
    def blue(s): return Color.colorize("blue", s)

    @staticmethod
    def gray(s): return Color.colorize("gray", s)

EX_OK              = 0
EX_WARNING         = 1
EX_CRITICAL        = 2
EX_UNKNOWN         = 3
EX_SKIPPED         = 4

def printable_outcome(outcome):
    if outcome == EX_OK:
        return "PASS"
    if outcome == EX_WARNING:
        return "WARN"
    if outcome == EX_CRITICAL:
        return "FAIL"
    if outcome == EX_UNKNOWN:
        return "UNKN"
    if outcome == EX_SKIPPED:
        return "SKIP"
    raise ValueError("invalid argument")

def colored_outcome(outcome):
    pt = printable_outcome(outcome)

    if outcome == EX_OK:
        return Color.green(pt)
    if outcome == EX_WARNING:
        return Color.yellow(pt)
    if outcome == EX_CRITICAL:
        return Color.red(pt)
    if outcome == EX_UNKNOWN:
        return Color.purple(pt)
    if outcome == EX_SKIPPED:
        return Color.yellow(pt)
    raise ValueError("invalid argument")

def count_printable_chars(text):
    return len(re.sub(r'\033\[[\d;]*m', '', text))

def determine_terminal_width():
    global ADVANCED_TTY
    if TTY_WIDTH is not None: return TTY_WIDTH
    if not ADVANCED_TTY: return 120

    try:
        return int(os.popen('stty size', 'r').read().split()[1])
    except:
        ADVANCED_TTY = False
        return 120

def terminal_align(left, right):
    width = determine_terminal_width()
    chars_left = count_printable_chars(left)
    chars_right = count_printable_chars(right)
    space = width - chars_left - chars_right

    if space < 0: space = 1

    return left + space * " " + right

def colorize_protocol(s):
    if s == "davs" or s == "dav" or s == "http" or s == "https":
        return Color.purple(s)
    if s == "xroot":
        return Color.blue(s)
    if s == "gsiftp":
        return Color.cyan(s)
    if s.startswith("gfal:"):
        return Color.yellow(s)
    return Color.gray(s)

def update_terminal_line(message):
    if ADVANCED_TTY:
        print('\x1b[2K\r', end="")
    else:
        print("") # newline
    print(message, end="\r")
    sys.stdout.flush()

def indented_print(s, indent="   ", increase = 4, bottomlevel = True):
    if not s: return
    if type(s) is list:
        for item in s:
            newindent = indent
            if not bottomlevel: newindent = indent + " "*increase
            indented_print(item, newindent, increase, False)
        return

    for line in textwrap.wrap(s, 120, subsequent_indent=indent, initial_indent=indent):
        print(line)

def duration_in_sec(start, end):
    delta = end - start
    return delta.seconds + (delta.microseconds / 1000000)

def nsec_to_sec(t):
    return t * 10**(-9)

def nsec_to_msec(t):
    return t * 10**(-6)

def path_join(p1, p2):
    return os.path.join(p1, p2)

def shellquote(l):
    assert type(l) is list
    return [quote(x) for x in l]

def duration_to_string(duration, initialization=None):
    if duration is None:
        return "None"
    if initialization is None:
        return "{0} sec".format(duration)

    return "init: {0} sec, main: {1} sec".format(initialization, duration)

class ProgressTicker:
    def __init__(self, message):
        self.message = message
        self.ndots = 0
        self.starttime = datetime.datetime.now()

        self.tick()

    def tick(self, status=""):
        if NO_TICKING: return

        now = datetime.datetime.now()
        dots = (self.ndots+1)*"." + " "*(3-self.ndots)
        self.ndots = (self.ndots+1)%4

        update_terminal_line(
            terminal_align(
                self.message.format(outcome=dots, duration=duration_to_string(int(duration_in_sec(self.starttime, now)))),
                status.strip()
            )
        )

    def finalize(self, outcome, initDuration, duration):
        if duration: duration = "{0:.2f}".format(duration)
        if initDuration: initDuration = "{0:.2f}".format(initDuration)
        update_terminal_line(self.message.format(outcome=colored_outcome(outcome), duration=duration_to_string(duration, initDuration)))
        print("")

def spawn(command, combine_stdout_stderr=False):
    assert type(command) is str
    if VERBOSITY > 1: print("Executing external shell command: {0}".format(command))

    std_err = subprocess.PIPE
    if combine_stdout_stderr: std_err = subprocess.STDOUT

    return subprocess.Popen(
        command,
        shell=True,
        stdout=subprocess.PIPE,
        stderr=std_err,
        close_fds=True,
        preexec_fn=os.setsid,
        encoding = "utf-8"
    )

class Runner(threading.Thread):
    def __init__(self, agent, options):
        super(Runner, self).__init__()
        self.agent = agent
        self.options = options
        self.command = self.agent.create_command_and_wrap(options)

        self.stdout = StringIO()
        self.stderr = StringIO()

        self.last_status_line = ""
        self.process = None
        self.active = None

        self.retcode = None
        self.active = True

    def output(self):
        return (self.stdout.getvalue(), self.stderr.getvalue())

    def finished(self):
        return not self.active

    def terminate(self):
        self.active = False

        try:
            os.killpg(os.getpgid(self.process.pid), signal.SIGTERM)
        except Exception as e:
            if e.errno != 3: raise

    def status(self):
        prefix = "[local] "
        if not self.agent.local(): prefix = "[{0}] ".format(self.agent.master.host)

        status = self.last_status_line
        if not self.active:
            if self.retcode == 0:
                status = "Finished"
            else:
                status = "Terminated with status {0}".format(self.retcode)

        return prefix + status

    def run(self):
        assert self.process is None
        self.process = spawn(self.command)

        out = "-"
        err = "-"
        while (self.process.poll() is None or out != "" or err != ""):
            if TERMINATE: break

            ready = select.select([self.process.stdout.fileno(), self.process.stderr.fileno()], [], [], 1)

            if self.process.stdout.fileno() in ready[0]:
                out = self.process.stdout.read()
                self.stdout.write(out)

            if self.process.stderr.fileno() in ready[0]:
                err = self.process.stderr.readline()
                if err:
                    self.last_status_line = err
                    self.stderr.write(err)

        if TERMINATE: self.terminate()
        self.retcode = self.process.wait()
        self.active = False

def ensure_valid_choice(parser, choice, text, available):
    if choice:
        if type(choice) == str: choice = [choice]

        for item in choice:
            if item not in available:
                parser.error("unrecognized {0}: '{1}'. Available choices: {2}".format(text, item, available))

def declare_incompatible_options(parser, option, group):
    if option not in sys.argv: return

    for item in group:
        if item in sys.argv:
            parser.error("argument {0} is incompatible with argument {1}".format(option, item))

def get_printable_rate(rate):
    if not rate: return "Rate: N/A"
    return "Rate: {0:0.2f} Hz".format(rate)

def get_printable_latency_span(latencyMin, latencyMax):
    if not latencyMin or not latencyMax: return "Latency span: N/A"
    return "Latency span: from {0:0.2f} to {1:0.2f} sec".format(latencyMin, latencyMax)

def get_printable_latency_outliers(outliers):
    if not outliers: return "Top latency outliers: N/A"
    return "Top latency outliers: " + ", ".join( ["#{0} ({1} ms)".format(item.filename, nsec_to_msec(item.latency)) for item in outliers])

def getargs():
    class CustomFormatter(argparse.ArgumentDefaultsHelpFormatter, argparse.RawDescriptionHelpFormatter):
        pass

    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1', 'on'):
            return True
        if v.lower() in ('no', 'false', 'f', 'n', '0', 'off'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    parser = argparse.ArgumentParser(formatter_class=CustomFormatter,
                                     description="Top-level script to orchestrate the performance tests.\nYou'll need the hammer-* binaries to run this.",
                                     epilog="There are two modes in which this script can be run, scenario-based or ad-hoc.\n"
                                     "1) In scenario-based mode, you select a scenario out of a pre-defined list.\n"
                                     "   - light: 1k files over https and xrootd, testing all operations over 3 runs, and [1, 4, 10, 100] threads\n"
                                     "   - heavy-read: 50k files, read and stat, 3 runs, [1, 4, 10, 100] threads. The files must exist beforehand.\n"
                                     "   - standard: same as above, but with 10k files and [1, 2, 4, 10, 20, 50, 100, 200, 500] threads\n"
                                     "2) In ad-hoc mode, you manually specify the parameters you want, but the visualizer might\n"
                                     "   not be able to produce full graphs out of the results.\n\n"
                                     "It is NOT safe to run this script against production systems.\n"
                                     "Unless you like to live dangerously.\n")

    parser.add_argument('--data-repo', type=str, required=False, default=None, help="The location of the data repository. Needs to exist beforehand - an empty folder will do. If not specified, results will not be persisted.")
    parser.add_argument('--url', type=str, required=True, help="The base remote path for the tests. The URL scheme is irrelevant, and will be replaced accordingly.")
    parser.add_argument('--binary-dir', type=str, help="The location where to look for the hammer-* binaries. If not specified, $PWD and $PATH will be used to locate them.")
    parser.add_argument('--cert', type=str, help="Client certificate in PEM format. If not specified, the standard grid paths will be searched.")
    parser.add_argument('--key', type=str, help="Private key in PEM format")
    parser.add_argument('--verbose', '-v', default=0, action="count", help="Verbose output")
    parser.add_argument('--strict-exit-code', type=int, default=0, help="The script exit code when the number of failed operations is greater than zero. Useful in CI, for example, when expecting no failed operations at all.")
    parser.add_argument('--initialization', type=str2bool, nargs='?', const=True, default=False, help="If true, the connections will be initialized in a separate step beforehand, instead of lazily upon thread creation. Useful to measure the impact of setting up the connections, usually originating from TCP handshake, TLS handshake, or kXR_login.")
    parser.add_argument('--constantnfiles', dest="constantnfiles", action="store_true", help="Keep the number of files constant, without scaling it on the chosen number of threads in a scenario run. May cause longer runs when the chosen number of threads is low.")
    parser.add_argument('--max-in-flight', type=int, default=1000, help="The maximum number of operations in-flight. Use a low value to get meaningful latency spans with many asynchronous operations.")

    group = parser.add_argument_group('pretty printing options')
    group.add_argument('--simple-tty', action='store_true', help="Disable colors and fancy output, assume simple TTY (ie file, or jenkins)")
    group.add_argument('--simple-tty-with-color', action='store_true', help="Assume simple TTY, but with color support.")
    group.add_argument('--ticking-interval', type=float, default=1, help="How often the ticker should be updated")
    group.add_argument('--tty-width', type=int, default=None, help="The width of the terminal. If not given, it will be guessed from the terminal settings, and if that's not possible, will be set to 120.")
    group.add_argument('--no-ticking', dest="no_ticking", action="store_true", help="If set, disable interactive ticking.")
    group.add_argument('--gitlab', dest="gitlab", action="store_true", help="Alias for --simple-tty-with-color and --no-ticking")

    group = parser.add_argument_group('distributed options')
    group.add_argument('--agents', type=str, nargs="+", help="The agent hosts on which to run the tests")
    group.add_argument('--no-local-agent', dest="no_local_agent", action="store_true", help="By default, the tests are also run on localhost, in addition to the agents above. Pass this flag to disable that")
    group.add_argument('--socket-path', type=str, default="/tmp", help="The folder in which to temporarily store the socket files for the SSH connections")
    group.set_defaults(no_local_agent=False)

    group.add_argument('--identity', type=str, help="The private key used to SSH into the agents")
    group.add_argument('--identity-pw', type=str, help="The password to the encrypted private key")
    group.add_argument('--ssh-args', type=str, nargs="+", help="Additional arguments to pass to the SSH command")

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--operations', type=str, nargs="+", help="The ad-hoc operations to perform")
    group.add_argument('--scenario', type=str, default="light", help="The test scenario to run", choices=["light", "standard", "heavy-read"])

    group = parser.add_argument_group('ad-hoc mode options')
    group.add_argument('--nfiles', type=int, default=1000, help="The number of files to use for the tests")
    group.add_argument('--runs', type=int, default=3, help="The number of runs to perform")
    group.add_argument('--threads', type=int, nargs="+", default=[1, 2, 10, 100], help="The number of threads to use")
    group.add_argument('--protocols', type=str, nargs="+", default=["http", "https", "dav", "davs", "xroot", "gfal:http", "gfal:https", "gfal:dav", "gfal:davs", "gfal:xroot", "gfal:gsiftp", "gfal:srm" ], help="The protocols to test")

    args = parser.parse_args()

    ensure_valid_choice(parser, args.protocols, "protocol", ["davs", "dav", "http", "https", "xroot", "gfal:http", "gfal:https", "gfal:dav", "gfal:davs", "gfal:xroot", "gfal:gsiftp", "gfal:srm"])
    ensure_valid_choice(parser, args.operations, "operation", ["stat", "read", "write", "delete", "append", "list", "liststat"])

    if "://" in args.url:
        parser.error("--url should not contain a scheme")

    if args.no_local_agent and not args.agents:
        parser.error("No --agents were specified; necessary, since --no-local-agent was given")

    declare_incompatible_options(parser, "--scenario", ["--nfiles", "--runs", "--threads"])
    declare_incompatible_options(parser, "--simple-tty", ["--simple-tty-with-color"])
    declare_incompatible_options(parser, "--no-ticking", ["--ticking-interval"])
    declare_incompatible_options(parser, "--gitlab", ["--simple-tty", "--simple-tty-with-color", "--no-ticking", "--ticking-interval"])

    global VERBOSITY
    VERBOSITY = args.verbose
    if VERBOSITY > 0: print("Enabling verbose output (level {0})".format(VERBOSITY))

    global USE_COLORS
    global ADVANCED_TTY
    if args.simple_tty:
        USE_COLORS = False
        ADVANCED_TTY = False

    if args.simple_tty_with_color:
        ADVANCED_TTY = False

    global TTY_WIDTH
    if args.tty_width:
        TTY_WIDTH = args.tty_width

    global TICKING_INTERVAL
    TICKING_INTERVAL = args.ticking_interval

    global NO_TICKING
    if args.no_ticking:
        NO_TICKING = True

    if args.gitlab:
        ADVANCED_TTY = False
        NO_TICKING = True
        USE_COLORS = True

    return args

class RunOptions(object):
    """A class that fully describes the configuration of a run"""
    def __init__(self, operationid, operation, url, protocol, firstfile, endfile, threads, run, initialization, maxInFlight):
        self.operationid = operationid
        self.operation = operation
        self.url = url
        self.protocol = protocol
        self.firstfile = firstfile
        self.endfile = endfile
        self.threads = threads
        self.run = run
        self.initialization = initialization
        self.maxInFlight = maxInFlight

class OperationOutcome(object):
    """A class to describe a single operation, ie single write, stat, read"""
    def __init__(self, filename, latency):
        self.filename = filename
        self.latency = latency

class RunResult(object):
    """Results from a single run"""
    def __init__(self, runner):
        self.retcode = runner.retcode
        self.stdout, self.stderr = runner.output()
        self.command = runner.command
        self.options = runner.options
        self.agentHost = runner.agent.host()

        self.rate = None
        self.initDuration = None
        self.duration = None
        self.parseError = None
        self.operations = None
        self.latencyMin = None
        self.latencyMax = None
        self.latencyOutliers = None

        try:
            self.parseOutput(self.stdout)
        except Exception as e:
            self.parseError = e
            

    def parseOutput(self, output):
        self.output = json.loads(output)
        self.duration = nsec_to_sec(self.output["duration"])

        if "init-duration" in self.output:
            self.initDuration = nsec_to_sec(self.output["init-duration"])

        self.errors = []
        for outcome in self.output["outcomes"]:
            if "error" in outcome: self.errors += [outcome["error"]]

        self.latencies = []
        for outcome in self.output["outcomes"]: self.latencies += [outcome["latency"]]
        self.latencies.sort()

        self.latencyOutliers = []
        for outcome in self.output["outcomes"]:
            if len(self.latencies) <= 3 or outcome["latency"] >= self.latencies[-3]:
                self.latencyOutliers.append(OperationOutcome(outcome["filename"], outcome["latency"]))

        self.latencyMin = nsec_to_sec(min(self.latencies))
        self.latencyMax = nsec_to_sec(max(self.latencies))
        self.operations = len(self.output["outcomes"])
        self.rate = self.operations / self.duration

        op = self.output.get("operation", "")
        if op == "LIST" or op == "LISTSTAT":
            lastfile = int(self.output.get("lastfile", 0))
            self.rate = (self.operations * lastfile) / self.duration

    def outcome(self):
        if self.retcode != 0: return EX_CRITICAL
        if self.parseError: return EX_CRITICAL

        if len(self.errors) > 0 and len(self.errors) < 10: return EX_WARNING
        if len(self.errors) >= 10: return EX_CRITICAL
        return EX_OK

    def errorSummary(self):
        ret = []
        if self.errors:
            ret += ["{0} operations failed out of {1}.".format(len(self.errors), len(self.output["outcomes"]))]

            shown = 0
            for error in self.errors:
                ret += [error]
                shown += 1

                if shown >= 10 and VERBOSITY == 0:
                    ret += ["Only the first 10 errors are shown."]
                    break
        return ret

    def statistics(self):
        ret = [get_printable_rate(self.rate)]
        ret += [get_printable_latency_span(self.latencyMin, self.latencyMax)]
        ret += [get_printable_latency_outliers(self.latencyOutliers)]
        return ret

    def summary(self):
        ret = []

        if VERBOSITY > 0 or self.retcode != 0:
            ret += ["Command: {0}\n".format(self.command)]

        if VERBOSITY > 0:
            ret += ["Assigned files [{0}-{1}) and {2} threads\n".format(self.options.firstfile, self.options.endfile, self.options.threads)]

        if self.retcode != 0:
            ret += ["Bad exit code: {0}.".format(self.retcode)]
            ret += ["stdout: {0}".format(self.stdout), "stderr: {0}".format(self.stderr)]
            return ret

        if self.parseError:
            ret += ["Unable to parse json: {0}".format(str(self.parseError))]
            return ret

        ret += self.errorSummary()

        if VERBOSITY > 0:
            ret += self.statistics()

        return ret

def sum_or_none(numberlist):
    if not numberlist or None in numberlist:
        return None
    return sum(numberlist)

def min_or_none(numberlist):
    if not numberlist or None in numberlist:
        return None
    return min(numberlist)

def max_or_none(numberlist):
    if not numberlist or None in numberlist:
        return None
    return max(numberlist)

def division_or_none(a, b):
    if a is None or b is None: return None
    return a / b

# combine results from a single multi-agent run
class AggregateResult:
    def __init__(self, results):
        self.results = results

        # Init duration for an aggregate result doesn't make sense, since
        # each has its own. Use only if we're aggregating a single RunResult
        self.initDuration = None
        if len(self.results) == 1:
            self.initDuration = self.results[0].initDuration

        self.duration = max_or_none([res.duration for res in self.results])
        self.operations = sum_or_none([res.operations for res in self.results])
        self.rate = division_or_none(self.operations, self.duration)

    def outcome(self):
        outcomes = [res.outcome() for res in self.results]
        return max(outcomes)

    def statistics(self):
        if len(self.results) == 1: return self.results[0].statistics()

        latencyMin = min(res.latencyMin for res in self.results)
        latencyMax = max(res.latencyMax for res in self.results)
        latencyOutliers = self.results[0].latencyOutliers # Ignore all but one.. fix

        ret = ["Rate: {0:0.2f} Hz\n".format(self.rate)]
        ret += ["Latency span: from {0:0.2f} to {1:0.2f} sec".format(latencyMin, latencyMax)]
        return ret

    def summary(self):
        ret = []
        for res in self.results:
            summary = res.summary()
            if summary: ret += ["Agent: {0}".format(res.agentHost), summary]

        ret += self.statistics()
        return ret

def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)

class ResultRecorder:
    """Stores benchmark results into the data repository"""
    def __init__(self, dataRepo, scenario):
        self.dataRepo = dataRepo
        self.scenario = scenario

        self.failures = 0
        if not self.dataRepo: return # no data repo, no recording of results

        self.timestamp = datetime.datetime.now()
        if not os.path.exists(self.dataRepo): raise Exception("Data repository does not exist: {0} (an empty folder would be OK)".format(self.dataRepo))

        self.invocationDir = path_join(self.dataRepo, self.timestamp.strftime("%Y-%m-%d_%H.%M.%S.%f"))
        mkdir(self.invocationDir)

        self.scenarioDir = path_join(self.invocationDir, scenario)
        mkdir(self.scenarioDir)

        self.record_invocation_info()
        self.set_state("RUNNING\n")

    def nfailures(self):
        return self.failures

    def set_state(self, state):
        if not self.dataRepo: return # no data repo, no recording of results
        with open(path_join(self.invocationDir, "state"), "w") as f:
            f.write(state)

    def record_invocation_info(self):
        with open(path_join(self.invocationDir, "runner-hash"), "w") as f:
            f.write(filehash(sys.argv[0]) + "\n")

        with open(path_join(self.invocationDir, "args"), "w") as f:
            f.write(str(sys.argv) + "\n")

        with open(path_join(self.invocationDir, "runner-host"), "w") as f:
            f.write(platform.node() + "\n")

    def record(self, runres):
        if runres.outcome() != EX_OK: self.failures += 1
        if not self.dataRepo: return # no data repo, no recording of results

        agentDir = path_join(self.scenarioDir, "{0}".format(runres.agentHost))
        mkdir(agentDir)

        protocolDir = path_join(agentDir, runres.options.protocol)
        mkdir(protocolDir)

        operationDir = path_join(protocolDir, "{0}.{1}".format(runres.options.operationid, runres.options.operation))
        mkdir(operationDir)

        threadDir = path_join(operationDir, "threads-{0}".format(runres.options.threads))
        mkdir(threadDir)

        runFile = path_join(threadDir, "run-{0}".format(runres.options.run))
        assert not os.path.exists(runFile)
        with open(runFile, "w") as f:
            f.write(runres.stdout)

class BenchmarkRunner(object):
    """A class that runs benchmarks"""
    def __init__(self, agents, recorder):
        self.agents = agents
        self.recorder = recorder

    def monitor_runners(self, runners, ticker):
        running = list(runners)
        finished = []

        for r in running: r.start()

        roundrobin = 0
        while True:
            if TERMINATE or running == []: break

            for runner in running:
                if runner.finished():
                    running.remove(runner)
                    finished.append(runner)

            choice = roundrobin
            roundrobin = (roundrobin + 1) % len(runners)
            ticker.tick(runners[choice].status())
            time.sleep(TICKING_INTERVAL)

        results = []
        for runner in running + finished:
            runner.join()
            results.append(RunResult(runner))
        return results

    # partition given options into the available agents
    def partition_work(self, options):
        nagents = len(self.agents)
        nfiles = options.endfile - options.firstfile
        if nagents == 1: return [Runner(self.agents[0], options)]

        files_per_thread = max(1, nfiles / options.threads)
        threads_per_agent = int(max(1, math.floor(options.threads / nagents)))

        firstfile = options.firstfile
        threads_used = 0

        runners = []
        for agent in self.agents:
            if threads_used >= options.threads:
                # I've already distributed all threads to the available
                # agents, but some agent will starve. Happens when
                # the number of agents is larger than the number of threads
                break

            opts = copy.deepcopy(options)

            threads_for_this_agent = threads_per_agent
            if threads_used + threads_per_agent > options.threads:
                # we're probably dealing with the tail now, so I cannot give
                # the average number of threads to this agent. Give what's left
                threads_for_this_agent = options.threads - threads_used

            threads_used += threads_for_this_agent
            firstfile_for_this_agent = firstfile

            if threads_used == options.threads:
                # no more threads - give out all remaining files, there might be a tail
                endfile_for_this_agent = options.endfile
            else:
                endfile_for_this_agent = firstfile + int(math.floor(threads_for_this_agent * files_per_thread))

            firstfile = endfile_for_this_agent

            opts.threads = threads_for_this_agent
            opts.firstfile = firstfile_for_this_agent
            opts.endfile = endfile_for_this_agent

            runners.append(Runner(agent, opts))

        return runners

    def perform(self, options, description):
        ticker = ProgressTicker(description)
        if TERMINATE:
            ticker.finalize(EX_SKIPPED, None, None)
            return

        runners = self.partition_work(options)
        results = self.monitor_runners(runners, ticker)

        if TERMINATE:
            ticker.finalize(EX_SKIPPED, None, None)
            return

        aggregate = AggregateResult(results)
        for result in results:
            self.recorder.record(result)

        ticker.finalize(aggregate.outcome(), aggregate.initDuration, aggregate.duration)
        indented_print(aggregate.summary(), " "*9)

def scenario_mode(agents, recorder, args):
    runner = BenchmarkRunner(agents, recorder)

    threads = [1, 4, 10, 100]
    nfiles = 1000
    runs = 2

    operations = ["write", "stat", "stat", "stat", "read", "delete"]

    if args.scenario == "heavy-read":
        operations = ["stat", "stat", "read"]
        nfiles = 50000
        threads = [1, 4, 10, 40, 100]

    if args.scenario == "standard":
        nfiles = 10000
        threads = [1, 2, 4, 10, 20, 50, 100, 200, 500]


    for nthreads in threads:
        for run in range(1, runs+1):
            print("{0} :: {1} :: {2}".format(
                Color.gray("{0} files".format(nfiles),),
                Color.yellow("{0} threads".format(nthreads)),
                Color.gray("Run {0}/{1}".format(run, runs)))
            )

            for initial_protocol in ["xroot", "davs"]:
                for operationid, operation in enumerate(operations):
                    protocols = [initial_protocol]

                    if initial_protocol == "davs" and (operation == "read"):
                        protocols = ["davs", "dav"]
                    if initial_protocol == "davs" and (operation == "stat"):
                        protocols = ["davs", "https", "dav", "http"]


                    for protocol in list( set(protocols) & set(args.protocols) ):
                        nfilesfinal = nfiles
                        if not args.constantnfiles:
                          # Try to avoid runs that are too long with small numbers of threads,
                          # by automatically scaling with the thread count
                          nfilesfinal = nfiles * nthreads / max(threads) * 3
                          # Shield against crazy combinations, who knows
                          nfilesfinal = min(nfiles, nfilesfinal)
                          nfilesfinal = max(nthreads/8, nfilesfinal)
                          nfilesfinal = int(nfilesfinal)
                        options = RunOptions(operationid, operation, args.url, protocol, 0, nfilesfinal, nthreads, run, args.initialization, args.max_in_flight)
                        description = "    {{outcome}} {0} :: {1} ({{duration}})".format(colorize_protocol(protocol), operation)
                        runner.perform(options, description)

def adhoc_mode(agents, recorder, args):
    runner = BenchmarkRunner(agents, recorder)

    for nthreads in args.threads:
        for run in range(1, args.runs+1):
            print("{0} :: {1} :: {2}".format(
                Color.gray("{0} files".format(args.nfiles)),
                Color.yellow("{0} threads".format(nthreads)),
                Color.gray("Run {0}/{1}".format(run, args.runs)))
            )

            for protocol in args.protocols:
                for operationid, operation in enumerate(args.operations):
                    options = RunOptions(operationid, operation, args.url, protocol, 0, args.nfiles, nthreads, run, args.initialization, args.max_in_flight)
                    description = "    {{outcome}} {0} :: {1} ({{duration}})".format(colorize_protocol(protocol), operation)
                    runner.perform(options, description)

TERMINATE = False
def shut_down_everything():
    global TERMINATE
    TERMINATE = True

def ctrlc(signal, frame):
    print("\nReceived ctrl-c, shutting down .. ")
    shut_down_everything()

def describe_scenario(args):
    if args.operations: return "ad-hoc-{0}-files".format(args.nfiles)
    return args.scenario

def filehash(path):
    with open(path) as f:
        contents = f.read()

    return hashlib.sha256(contents).hexdigest()

class MasterSSHConnection(threading.Thread):
    def __init__(self, host, socket, sshargs = []):
        super(MasterSSHConnection, self).__init__()

        self.host = host
        self.socket = socket
        self.sshargs = sshargs

        loop = "while true; do echo PING; sleep 1; done"
        print("ssh -M {0} -S {1} {2} '{3}'".format(" ".join(self.sshargs), self.socket, self.host, loop))
        self.process = spawn(
          "ssh -M {0} -S {1} {2} '{3}'".format(" ".join(self.sshargs), self.socket, self.host, loop),
          combine_stdout_stderr=True
         )

        self.assertPing(self.process.stdout.readline())
        self.active = True

    def assertPing(self, out):
        if out != "PING\n":
            global TERMINATE
            print("Aborting run, unexpected output when establishing a master SSH connection to {0}: {1}".format(self.host, out))
            TERMINATE = True

    def terminate(self):
        self.active = False

        try:
            os.killpg(os.getpgid(self.process.pid), signal.SIGTERM)
        except Exception as e:
            if e.errno != 3: raise

    def run(self):
        global TERMINATE

        now = time.time()
        out = "-"
        while (self.process.poll() is None or out != ""):
            if TERMINATE: break
            self.assertPing(self.process.stdout.readline())

            prev = now
            now = time.time()

            if now - prev >= 1.5:
                print("WARNING: latency towards {0} reached {1}".format(self.host, now-prev))
            if now - prev >= 3:
                print("Aborting run because connection to agent {0} is unstable".format(self.host))
                TERMINATE = True

        if TERMINATE: self.terminate()
        self.retcode = self.process.wait()
        self.active = False

    # wrap a command for execution on the remote host
    def wrap_command(self, command, shell=False):
        assert self.active
        if not shell: command = " ".join(shellquote(command))
        return "ssh {0} -S {1} {2} {3}".format(" ".join(self.sshargs), self.socket, self.host, command)

class Agent(object):
    def __init__(self, binaryDir, protocols, host = None, socket = None, sshargs = [], cert = None, key = None):
        self.binaryDir = binaryDir
        self.protocols = protocols
        self.cert = cert
        self.key = key

        if not host:
            # "launching" a local agent
            self.master = None
        else:
            # establish a master SSH connection
            self.master = MasterSSHConnection(host, socket, sshargs)
            self.master.start()

        self.locateBinaries()

    def describe(self):
        if self.master: return "agent {0}".format(self.master.host)
        return "localhost"

    def locateBinaries(self):
        self.protocolmap = {}

        for protocol in self.protocols:
            if protocol in ["davs", "dav", "http", "https"]: protocol = "http"
            if protocol.startswith("gfal:"): protocol = "gfal"
            if protocol in self.protocolmap: continue

            executable = self.locateHammerBinary(self.binaryDir, protocol)
            if not executable:
                print("Could not locate hammer binary for protocol {0} on {1}".format(protocol, self.describe()))
                sys.exit(1)

            if VERBOSITY > 0: print("Hammer for {0} found in {1}:{2}".format(protocol, self.host(), executable))
            self.protocolmap[protocol] = executable

    def find_executable(self, executable, paths):
        path = ""
        if paths:
            for p in paths: path += "'{0}':".format(p)
            path = "PATH={0}$PATH".format(path)

        process = self.spawn("{0} which {1}".format(path, executable), shell=True)
        retcode = process.wait()

        if retcode != 0: return None
        return process.stdout.read().strip()

    def locateHammerBinary(self, binaryDir, protocol):
        executable = "hammer-{0}".format(protocol)

        locations = []
        if binaryDir: locations = [binaryDir]
        locations += [os.getcwd(), path_join(os.getcwd(), "src")]

        location = self.find_executable(executable, locations)

        # If binaryDir is specified, then fail to start if binaries are not there,
        # even if they are available in $PATH or $PWD.
        # This way, an explicit binary dir will never be silently ignored.
        if location and binaryDir and not location.startswith(binaryDir + "/"):
            return None

        return location

    def wrap_command(self, command, shell=False):
        if self.master: return self.master.wrap_command(command, shell)
        if not shell: command = " ".join(shellquote(command))
        return command

    def spawn(self, command, shell=False):
        wrapped = self.wrap_command(command, shell)
        return spawn(wrapped)

    def host(self):
        if self.local(): return platform.node()
        return self.master.host

    def local(self):
        return self.master is None

    def create_command_and_wrap(self, options):
        return self.wrap_command(self.create_command(options))

    def create_command(self, options):
        urlprotocol = options.protocol
        if urlprotocol.startswith("gfal:"): urlprotocol = urlprotocol[len("gfal:"):]
        if urlprotocol == "xroot": urlprotocol = "root"

        hammerprotocol = options.protocol
        if hammerprotocol.startswith("gfal:"): hammerprotocol = "gfal"
        if hammerprotocol in ["davs", "dav", "http", "https"]: hammerprotocol = "http"

        url = urlprotocol + "://" + options.url
        binary = self.protocolmap[hammerprotocol]

        command = [binary]
        command += ['--url', url]
        command += ['--nthreads', str(options.threads)]

        if self.cert:
            command += ['--cert', self.cert]
        if self.key:
            command += ['--key', self.key]

        command += ['--operation', options.operation]
        command += ['--firstfile', str(options.firstfile)]
        command += ['--lastfile', str(options.endfile)]
        command += ['--initialization', str(options.initialization).lower()]
        command += ['--max-in-flight', str(options.maxInFlight)]
        return command

def initializeAgents(args):
    ret = []
    if not args.no_local_agent: ret += [Agent(args.binary_dir, args.protocols, cert=args.cert, key=args.key)]

    if args.agents:
        for host in args.agents:
            binary_dir = args.binary_dir
            if ":" in host:
                parts = host.split(":")
                host = parts[0]
                binary_dir = parts[1]

            socketpath = path_join(args.socket_path, host)
            ret += [Agent(binary_dir, args.protocols, host, socketpath, args.ssh_args, args.cert, args.key)]

    return ret

def main():
    retcode = 0
    global TERMINATE

    try:
        signal.signal(signal.SIGINT, ctrlc)
        args = getargs()
        recorder = ResultRecorder(args.data_repo, describe_scenario(args))
        agents = initializeAgents(args)

        if args.operations:
            adhoc_mode(agents, recorder, args)
        else:
            scenario_mode(agents, recorder, args)

        if TERMINATE:
            recorder.set_state("CANCELLED\n")
            retcode = 1
        else:
            recorder.set_state("SUCCESS\n")
            if recorder.nfailures() != 0: retcode = args.strict_exit_code

        TERMINATE = True
    except Exception as e:
        shut_down_everything()

        if 'recorder' in vars():
            recorder.set_state("EXCEPTION\n{0}".format(e))
        raise

    return retcode

if __name__ == '__main__':
    retcode = main()
    sys.exit(retcode)
