#!/usr/bin/env bash
set -e

git submodule update --init --recursive
mkdir build && cd build

if which cmake3; then
export CMAKE=`which cmake3`
else
export CMAKE=`which cmake`
fi

${CMAKE} .. -DPACKAGEONLY=1
make srpm

if which dnf; then
  dnf builddep -y SRPMS/*
else
  yum-builddep -y SRPMS/*
fi

rpmbuild --rebuild --with server --define "_build_name_fmt %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm" SRPMS/*
