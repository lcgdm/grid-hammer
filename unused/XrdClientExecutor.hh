// --------------------------------------------------------------------------
// File: XrdClientExecutor.hh
// Author: Georgios Bitzes - CERN
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#ifndef __GRID_HAMMER_XRDCLIENT_EXECUTOR_H__
#define __GRID_HAMMER_XRDCLIENT_EXECUTOR_H__

#include <XrdClient/XrdClient.hh>
#include <XrdClient/XrdClientAdmin.hh>
#include <XrdClient/XrdClientEnv.hh>

#include "OperationExecutor.hh"

class XrdClientExecutor : public OperationExecutor {
public:
  ~XrdClientExecutor();

  virtual boost::unique_future<OperationOutcome> performSingle(
    Operation::Type op,
    const std::string &filename,
    const std::string &expectedContent,
    const int threadID
  );

  XrdClientExecutor(
    const std::string &cert,
    const std::string &key,
    const std::string &password
  );

  void initialize(int nthreads, const std::string &url);
private:
  std::string cert, key, password;

  std::map<size_t, XrdClientAdmin*> cds;


  XrdClientAdmin::XrdClientAdmin* createClientAdmin(int threadID, const std::string &url);
  XrdClientAdmin::XrdClientAdmin* obtainClientAdmin(int threadID, const std::string &url);
  void releaseClientAdmin(XrdClientAdmin::XrdClientAdmin *instance);

  std::deque<XrdClientAdmin::XrdClientAdmin*> clientAdminPool;
  boost::mutex poolMtx;
  boost::mutex createMtx;
};

#endif
