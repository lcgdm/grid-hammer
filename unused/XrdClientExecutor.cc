// --------------------------------------------------------------------------
// File: XrdClientExecutor.cc
// Author: Georgios Bitzes - CERN
// --------------------------------------------------------------------------

/****************************************************************************
 * Copyright (C) 2016 CERN/Switzerland                                      *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License");          *
 * you may not use this file except in compliance with the License.         *
 * You may obtain a copy of the License at                                  *
 *                                                                          *
 *     http://www.apache.org/licenses/LICENSE-2.0                           *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ***************************************************************************/

#include "XrdClientExecutor.hh"

#include <XrdClient/XrdClient.hh>
#include <XrdClient/XrdClientAdmin.hh>
#include <XrdClient/XrdClientEnv.hh>

XrdClientExecutor::XrdClientExecutor(const std::string &cert_, const std::string &key_, const std::string &password_)
: cert(cert_), key(key_), password(password_) {
}

XrdClientExecutor::~XrdClientExecutor() {
}

boost::unique_future<OperationOutcome> XrdClientExecutor::performSingle(
  Operation::Type op,
  const std::string &filename,
  const std::string &expectedContent,
  const int threadID
) {
  OperationOutcome ret;
  XrdClientAdmin::XrdClientAdmin *clientAdmin = obtainClientAdmin(threadID, filename);
  XrdCl::URL url(filename);

  struct timespec start;
  clock_gettime(CLOCK_MONOTONIC, &start);

  switch(op) {
    case Operation::STAT: {
      long id, flags, modtime;
      long long size;

      bool success = clientAdmin->Stat(url.GetPath().c_str(), id, size, flags, modtime);
      if(!success) {
        ret.error = SSTR("Stat towards " << filename << " failed, reason unknown.");
      }
      else if((size_t) size != expectedContent.size()) {
        ret.error = SSTR("File size mismatch for " << filename << ": '" << size << ", expected " << expectedContent.size());
      }
      break;
    }
    default:
      THROW("operation not implemented");
  }

  struct timespec end;
  clock_gettime(CLOCK_MONOTONIC, &end);

  ret.type = op;
  ret.latencyInNs = time_difference(start, end);

  releaseClientAdmin(clientAdmin);

  boost::promise<OperationOutcome> promise;
  promise.set_value(ret);

  if(ret.error.empty()) {
    progressTracker.addSuccess();
  }
  else {
    progressTracker.addFailure();
  }
  return promise.get_future();
}

XrdClientAdmin::XrdClientAdmin* XrdClientExecutor::createClientAdmin(int threadID, const std::string &url) {
  XrdCl::URL xrdurl(url);
  xrdurl.SetUserName(SSTR("thread-" << threadID));

  boost::unique_lock<boost::mutex> lock(createMtx);
  XrdClientAdmin *inst = new XrdClientAdmin::XrdClientAdmin(xrdurl.GetURL().c_str());
  inst->Connect();
  return inst;
}

XrdClientAdmin::XrdClientAdmin* XrdClientExecutor::obtainClientAdmin(int threadID, const std::string &url) {
  boost::unique_lock<boost::mutex> lock(poolMtx);

  if(cds.count(threadID) == 0) {
    lock.unlock();
    XrdClientAdmin::XrdClientAdmin *inst = createClientAdmin(threadID, url);
    lock.lock();

    cds[threadID] = inst;
    std::cerr << "Created for " << threadID << std::endl;
  }

  return cds[threadID];


  // if(!clientAdminPool.empty()) {
  //   XrdClientAdmin::XrdClientAdmin* ret = clientAdminPool.front();
  //   clientAdminPool.pop_front();
  //   return ret;
  // }
  //
  // lock.unlock();
  // return createClientAdmin(threadID, url);
  // // return createClientAdmin(url);
}

void XrdClientExecutor::releaseClientAdmin(XrdClientAdmin::XrdClientAdmin* instance) {
  // boost::unique_lock<boost::mutex> lock(poolMtx);
  // clientAdminPool.push_back(instance);
}

void XrdClientExecutor::initialize(int nthreads, const std::string &url) {
  for(int i = 0; i < nthreads; i++) {
    createClientAdmin(i, url);
  }
}
